var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose')
require('dotenv').config()

console.log(process.env.TEST)

var departmentRouter = require('./routes/departments');
var usersRouter = require('./routes/users');
const teamsRouter = require('./routes/teams')
const defaultRouter = require('./routes/index')

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/departments', departmentRouter);
app.use('/users', usersRouter);
app.use('/teams', teamsRouter);
app.use('/', defaultRouter)

try {
  const password = process.env.MONGODB_PASSWORD
  console.log(password)
  const uri = `mongodb+srv://Shortit:${password}@shortit.c2zyd.mongodb.net/shortit?retryWrites=true&w=majority`;
  mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log("connected to Mongo..."))
    .catch(err => console.log("could not connect to db", err))
  
} catch (error) {
  console.log(error)
}




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

const port = process.env.PORT || 3000
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
