const mongoose = require('mongoose')

const departmentSchema = new mongoose.Schema({
    department_id: integer 
    name: string
    teams: list < team_id > (nullable)
    inCharge: <user_id>
})

const Department = mongoose.model('department', departmentSchema)

module.exports = Department