const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    user_id: integer,
    name: string
})

const User = mongoose.model('user', userSchema)

module.exports = User