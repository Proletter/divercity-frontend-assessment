## **Question 1:**

How would you speed up a single page application?
Answers:

- By serving it using a CDN service e.g Azure static web apps or Aws Amplify
- Lazy loading of big assets and Lazy rendering.
- Static contnet caching on the client side
- Minifying modules/js scripts

## **Question 2:**

Explain the `this` keyword and it's quirks in JavaScript
The this keyword refers to the "owner" of the function one is executing.
Executing this a method on an object e.g obj.method(), "this" will return a reference to the object.
However if it is called standalone it points to the global object in node and window object in the browser.
You can circumvent these quirks by using the arrow function as it has no "this" binding and gets its "this" from its lexical scope.

## **Question 3:**

What is the difference between border-box and content-box?
Using border-box as the box-sizing for an element means that width and height properties would include the content, padding, and border, but do not include the margin.
Content-box on the other hand is the initial and default value as specified by the CSS standard. The width and height properties include the content, but does not include the padding, border, or margin.

## **Question 4:**

What's the difference between == and === operators?
The "==" is the equality operator while the "===" is the strict equality operation.
While the equality operator allows type coercion while comparing values the strict equality operator doesn't allow type coercion.

## **Question 5:**

What is accessibility? How do you achieve it?
Acessibility simply means designing your webapp so it becomes usable by almost everyone including those with disabilities. It is short of a11y.
https://www.w3.org/WAI/tips/designing/ has a comprehensive documentation on how to do this.
Things such as sufficient contrast beween content, providing correct associated labels for form elements etc are encouraged.
These things should be implemented in sites to allow screen readers easily convey the information on the site to people with disabilities

## **Question 6:**

What is the difference between session storage, local storage and cookies?
The main difference between Local and Session storage is that Local Storage has no expiration date while Session Storage data are gone when you close the browser tab - hence the name "session". Both storages are accessible via Javascript DOM. Cookies on the other hand is smaller and send server information back with every HTTP request as compare to local and session storage which can hold information on the client side.

## **Question 7:**

How does hoisting work in JavaScript, and what is the order of hoisting?
Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their scope before code execution. Inevitably, this means that no matter where functions and variables are declared, they are moved to the top of their scope regardless of whether their scope is global or local. Functions are hoisted first and then variable declarations next.

## **Question 8:**

What is the difference in usage of callback, promise, and async/await?
A callback is simply a function that gets called after an operation has been completed. This is achieved by passing a function as an argument into another function. Then calling that function with the return value of the original function. This was too clunky and would usually lead to callback hell should there be too many nested operations and it made code almost unreadable. Promises are improvements over callbacks and were designed to solve the problem of callback hell. When a promise is created it is in 3 states whicha re fulfilled, rejected or pending. Async await is syntactic sugar over promises. The await keyword can only be used in an async function. The operation pauses when it gets to the await keyword and ensures a return value exists before it continues its execution. All three are just ways of dealing with asynchronous operations in javascript.
