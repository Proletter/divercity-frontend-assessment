## **Question 1:**

Why us React over other JS frameworks?

Anewer: React Js as compared to other frameworks has a huge community and finding help is easy. It also doesn't have a steep learning
curve like Angular but is robust enough to fulfill almost all of one's development needs. It is backed by Facebook which means there
would be support for the framework for as long as Facebook exists.

## **Question 2:**

What happens during the lifecycle of a React component?

Each component in React has a lifecycle which you can monitor and manipulate during its three main phases. The three phases are: Mounting, Updating, and Unmounting.
Mounting is when the component is mounted, when the component is updated due to a change in state, and unmounting is when the component is destroyed. You can use the component lifecycle hooks
to listen for these events and modify the components based on these events.

## **Question 3:**

What are stateless components?

Stateless components are components that are unable to keep track of their state and are usually dumb components that simply receive props from a parent component
and then render said props. Functional components used to be called stateless components until the advent of react hooks which allows functional components to track their own state.

## **Question 4:**

Differentiate between Real DOM and Virtual DOM.

The Real dom is the dom provided by the browser itself. The virtual Dom is how React interacts with the Real Dom. This ensures that the real Dom is only touched when there is need to.
React compares the virtual Dom with the real Dom and checks if there are any differences. It then updates the Real Dom with the missing pieces of the virtual Dom.

## **Question 5:**

How would you improve SEO of a react Application?

- Server-side rendering
- Pre-rendering pages
- Using Nextjs
- Using libraries like React Helmet to add meta tags

## **Question 6:**

What are the limitations of React?

React is not a full fledged framework like Angular. This means it only handles the view part. Other features such as routing, authentication etc are all handled by third party libraries.
This means that one would have to make decisions on what library is the best one to use during development. This defeats the purpose of using a framework in the first place.

## **Question 7:**

What are some advantages of using React?

- It facilitates the overall process of writing components
- It boosts productivity and facilitates further maintenance
- It ensures faster rendering
- It guarantees stable code
- It is SEO friendly
- It comes with a helpful developer toolset.

## **Question 8:**

What is server-side rendering, and what problems does it solve?

Server-side rendering (SSR) is a popular technique for rendering a client-side single page application (SPA) on the server and then sending a fully rendered page to the client. This allows for dynamic components to be served as static HTML markup. It resolves the issues with SEO. Since the pages are in html already, indexing and crawling them would be more efficient.

## **Question 9:**

List and briefly describe some security attacks on the frontend?.

- XSS (Cross-site scripting) - XSS is a type of attack in which an attacker inputs a malicious script into the web application. When other users access the web application, since the browser does not know that it is malicious as it was served by the backend, this malicious script is executed.
- CSRF (Cross-Site Request Forgery) - CSRF is an attack that tricks the victim into submitting a malicious request.
